# [Valinor](https://gitlab.com/GCSBOSS/valinor)

A library for writing uncluttered, easy to read field and schema validations.

## Get Started

Install with: `npm i -P valinor`

In your code:

```js
const { v } = require('valinor');

// Validate one variable in place.
let result = v.int.between(0, 32).test(33);
console.log(result);

// Reuse one valinor.
let goodPwd = v.str.min(8);
console.log(
    goodPwd.test('abc'),
    goodPwd.test('abcdefgh'),
    goodPwd.test(null)
);

// Define a object schema valinor.
let schema = v.schema({
    name: v.str.match(/^[A-Z][a-z]+\ [A-Z][a-z]+$/),
    password: v.str.min(8),
    birth: v.date.min('2001-01-01').past
});

result = schema.test({
    name: 'John Doe',
    password: 'abcdefgh',
    birth: new Date('1999-01-01')
});

// Ignore empty/null values on optional valinors.
let schemaWithOpt = v.schema({
    firstName: v.str,
    lastName: v.opt.str
});

console.log(schemaWithOpt.test({
    firstName: 'John',
    lastName: null
}));

// Ensure you get only valdated keys of an object.
console.log(schemaWithOpt.test({
    firstName: 'John',
    unvalidated: 'This should not show up',
    lastName: 'Doe'
}).final);

// Define default values to show up in final object instead of blank optionals
let schemaWithOpt = v.schema({
    firstName: v.str,
    lastName: v.opt.str.def('Doe')
});

console.log(schemaWithOpt.test({
    firstName: 'John'
}).final);

```

`v.test()` method output is always an object with the following keys:

- `ok`: Whether validation passed.
- `final`: The input value after any modifications made by the Valinor. `undefined` unless all passed.
- `errs`: Array containing all found validation errors. `undefined` if all passed.

Check the [full rule reference](#rule-reference) for all the validations available.

## Reporting Bugs
If you have found any problems with this module, please:

1. [Open an issue](https://gitlab.com/GCSBOSS/valinor/issues/new).
2. Describe what happened and how.
3. Also in the issue text, reference the label `~bug`.

We will make sure to take a look when time allows us.

## Proposing Features
If you wish to get that awesome feature or have some advice for us, please:
1. [Open an issue](https://gitlab.com/GCSBOSS/valinor/issues/new).
2. Describe your ideas.
3. Also in the issue text, reference the label `~proposal`.

## Contributing
If you have spotted any enhancements to be made and is willing to get your hands
dirty about it, fork us and
[submit your merge request](https://gitlab.com/GCSBOSS/valinor/merge_requests/new)
so we can collaborate effectively.

## Method Reference

- `test ( input )`: Returns true if the input is valid. Returns an array or problems otherwise.

## Rule Reference

- `in ( array )`: Check if value can be found inside given array.
- `num`: Check if value is a number.
- `numeric`: Check if value is a number or a numeric string.
- `bool`: Check if value is a boolean.
- `boolLike`: Check if value is a boolean or a boolean-like value.
- `date`: Check if value is a date.
- `isoDate`: Check if value is a ISO date string.
- `obj`: Check if value is an object. Will fail for arrays, dates and JSON strings.
- `str`: Check if value is a string.
- `arr`: Check if value is array. Will fail for JSON strings.
- `match ( regex )`: Check if value matches the given regex.
- `gt ( primitive | date | array )`: Check if value or it's length is greater than subject.
- `lt ( primitive | date | array )`: Check if value or it's length is less than subject.
- `min ( primitive | date | array )`: Check if value or it's length is equal or greater than the subject.
- `max ( primitive | date | array )`: Check if value or it's length is equal or less than the subject.
- `between ( primitive | date | array )`: Check if value or it's length is inclusively inside the given range.
- `len ( string | array )`: Check if length is exactly the given subject.
- `has ( string | object | array )`: Check if the value contains the subject. For string, object and array it checks respectively substring, keys and values.
- `future ( date )`: Check if a date is in the future.
- `past ( date )`: Check if a date is in the past.
- `today ( date )`: Check if a date time is today.
- `schema ( object )`: Check if object's keys follows all their respective Valinor rules.
- `every ( array )`: Check if all array elements match a given Valinor rules.
- `some ( array )`: Check if any values of the array match given rules. Also picks only the valid values.
- `opt`: Mark the Valinor as optional, so skipping any validations for null, undefined or '' values.
- `not ( Valinor )`: Negates the given rules.
- `dif ( primitive | date | array )`: Check if values are sctrictly different. In Arrays, consider also order of elements.
- `def ( value )`: Set a default value to show up on `final` object in case the valinor is optional and validates an empty value.
- `alter ( fn )`: Mutates the input value acording to given funcion (`fn`) in a pipeline fashion before it reaches the next rule.
- `values ( Valinor )`: Check if all value in an object match given rules.
- `keys ( Valinor )`: Check if all keys in an object match given rules.