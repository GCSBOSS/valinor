const assert = require('assert');
const { v } = require('../lib/main.js');

describe('Valinor', function(){

    it('Should ignore other validations when optional field is blank', function(){
        assert.strictEqual(v.opt.int.test('').ok, true);
    });

    it('Should abort single valinor chain when rule check fails', function(){
        assert.strictEqual(v.str.between(2, 4).test('123').ok, true);
        assert.strictEqual(v.str.between(2, 4).test(undefined).ok, false);
    });

    it('Should be fail when wrong parameter type is sent to a rule', function(){
        assert.throws( () => v.in(4).test(false) );
        assert.throws( () => v.len(4).test(false) );
        assert.throws( () => v.has(4).test(false) );
    });

    describe('Type Checking', function(){

        it('Should validate integers', function(){
            assert.strictEqual(v.int.test(0).ok, true);
            assert.strictEqual(v.int.test('a').ok, false);
            assert.strictEqual(v.not(v.int).test('a').ok, true);
            assert.strictEqual(v.not(v.int).test(1).ok, false);
        });

        it('Should validate numeric strings', function(){
            assert.strictEqual(v.numeric.num.test(15).ok, true);
            assert.strictEqual(v.numeric.num.test('12.5').ok, true);
            assert.strictEqual(typeof v.numeric.test('100').final, 'number');
            assert.strictEqual(v.numeric.test('a').ok, false);
        });

        it('Should validate booleans', function(){
            assert.strictEqual(v.bool.test(true).ok, true);
            assert.strictEqual(v.bool.test('a').ok, false);
            assert.strictEqual(v.not(v.bool).test('a').ok, true);
            assert.strictEqual(v.not(v.bool).test(false).ok, false);
        });

        it('Should validate boolean-like values', function(){
            assert.strictEqual(v.boolLike.bool.test(true).ok, true);
            assert.strictEqual(v.boolLike.bool.test('true').ok, true);
            assert.strictEqual(v.boolLike.bool.test('false').ok, true);
            assert.strictEqual(v.boolLike.bool.test('1').ok, true);
            assert.strictEqual(v.boolLike.bool.test('0').ok, true);
            assert.strictEqual(v.boolLike.bool.test(1).ok, true);
            assert.strictEqual(v.boolLike.bool.test(0).ok, true);
            assert.strictEqual(typeof v.boolLike.test('false').final, 'boolean');
            assert.strictEqual(v.boolLike.test('2').ok, false);
            assert.strictEqual(v.boolLike.test(2).ok, false);
            assert.strictEqual(v.boolLike.test('foobar').ok, false);
        });

        it('Should validate strings', function(){
            assert.strictEqual(v.str.test('a').ok, true);
            assert.strictEqual(v.str.test(true).ok, false);
            assert.strictEqual(v.not(v.str).test(false).ok, true);
            assert.strictEqual(v.not(v.str).test('a').ok, false);
        });

        it('Should validate numbers', function(){
            assert.strictEqual(v.num.test(1.2).ok, true);
            assert.strictEqual(v.num.test('a').ok, false);
            assert.strictEqual(v.not(v.num).test('a').ok, true);
            assert.strictEqual(v.not(v.num).test(4).ok, false);
        });

        it('Should validate dates', function(){
            assert.strictEqual(v.date.test('12/12/2019').ok, true);
            assert.strictEqual(v.date.test('a').ok, false);
            assert.strictEqual(v.not(v.date).test('a').ok, true);
            assert.strictEqual(v.not(v.date).test(new Date()).ok, false);
        });

        it('Types - Should validate ISO string dates', function(){
            assert.strictEqual(v.date.test('2019-10-10T10:10:10Z').ok, true);
            assert.strictEqual(v.date.test('2019-10-10T10:10:10Z').final.constructor.name, 'Date');
            assert.strictEqual(v.date.test('2019-24-10T10:10:10Z').ok, false);
            assert.strictEqual(v.date.test('2019-xxxxxxx').ok, false);
        });

        it('Should validate objects', function(){
            assert.strictEqual(v.obj.test({}).ok, true);
            assert.strictEqual(v.obj.test('a').ok, false);
            assert.strictEqual(v.not(v.obj).test([]).ok, true);
            assert.strictEqual(v.not(v.obj).test({}).ok, false);
        });

        it('Should validate arrays', function(){
            assert.strictEqual(v.arr.test([]).ok, true);
            assert.strictEqual(v.arr.test('a').ok, false);
            assert.strictEqual(v.not(v.arr).test({}).ok, true);
            assert.strictEqual(v.not(v.arr).test([1]).ok, false);
        });

    });

    describe('Primitive Validations', function(){

        it('Should validate number greater than', function(){
            assert.strictEqual(v.gt(0).test(1).ok, true);
            assert.strictEqual(v.gt(2.2).test(1).ok, false);
        });

        it('Should validate number less than', function(){
            assert.strictEqual(v.lt(2).test(1.5).ok, true);
            assert.strictEqual(v.lt(1).test(1.5).ok, false);
        });

        it('Should validate number greater than or equal to', function(){
            assert.strictEqual(v.min(0).test(0).ok, true);
            assert.strictEqual(v.min(2.2).test(1).ok, false);
        });

        it('Should validate number less than or equal to', function(){
            assert.strictEqual(v.max(2).test(2).ok, true);
            assert.strictEqual(v.max(1).test(1.5).ok, false);
        });

        it('Should validate difference', function(){
            assert.strictEqual(v.dif('a').test(2).ok, true);
            assert.strictEqual(v.dif('a').test('a').ok, false);
        });

        it('Should validate if value is contained in array', function(){
            assert.strictEqual(v.in(['a', 'b', 'c']).test('b').ok, true);
            assert.strictEqual(v.in(['a', 'b']).test('c').ok, false);
            assert.strictEqual(v.not(v.in(['a', 'c'])).test('b').ok, true);
            assert.strictEqual(v.not(v.in(['a', 'b'])).test('a').ok, false);
        });

        it('Should validate if value is key of object', function(){
            assert.strictEqual(v.in({ a: true, b: 1, c: '' }).test('b').ok, true);
            assert.strictEqual(v.in({ a: true, b: 1 }).test('c').ok, false);
            assert.strictEqual(v.not(v.in({ a: true, c: '' })).test('b').ok, true);
            assert.strictEqual(v.not(v.in({ a: true, b: 1 })).test('a').ok, false);
        });

    });

    describe('Date Validations', function(){

        it('Should validate different dates', function(){
            let d = new Date(new Date().getTime() + 60000);
            assert.strictEqual(v.dif(d).test(new Date()).ok, true);
            assert.strictEqual(v.dif(d).test(d).ok, false);
        });

        it('Should validate string dates', function(){
            assert.strictEqual(v.date.test('2000-10-10').ok, true);
            assert.strictEqual(v.date.test('2000-10-10').final.constructor.name, 'Date');
        });

        it('Should validate past dates', function(){
            assert.strictEqual(v.past.test('2000-10-10').ok, true);
            assert.strictEqual(v.past.test('9999-02-02').ok, false);
        });

        it('Should validate future dates', function(){
            assert.strictEqual(v.future.test('9999-02-02').ok, true);
            assert.strictEqual(v.future.test('2000-10-10').ok, false);
        });

        it('Should validate dates in the present day', function(){
            let od = new Date(new Date().getTime() + 60000);
            assert.strictEqual(v.today.test(od).ok, true);
            assert.strictEqual(v.today.test('2000-10-10').ok, false);
        });

        it('Should validate date before another', function(){
            let d = new Date();
            assert.strictEqual(v.max('9999-02-02').test(d).ok, true);
            assert.strictEqual(v.max('2000-10-10').test(d).ok, false);
        });

        it('Should validate date after another', function(){
            let d = new Date();
            assert.strictEqual(v.min('2000-10-10').test(d).ok, true);
            assert.strictEqual(v.min('9999-02-02').test(d).ok, false);
        });

        it('Should validate date in range', function(){
            let d = new Date(new Date().getTime() - 60000);
            assert.strictEqual(v.between('2000-10-10', '9999-02-02').test(d).ok, true);
            assert.strictEqual(v.between(new Date().ok, '9999-02-02').test(d).ok, false);
        });

    });

    describe('String Validations', function(){

        it('Should validate substrings', function(){
            assert.strictEqual(v.has('a').test('bab').ok, true);
            assert.strictEqual(v.has('b').test('cac').ok, false);
        });

        it('Should validate superstrings', function(){
            assert.strictEqual(v.in('bab').test('a').ok, true);
            assert.strictEqual(v.in('cac').test('b').ok, false);
            assert.strictEqual(v.not(v.in('bab')).test('c').ok, true);
            assert.strictEqual(v.not(v.in('dad')).test('d').ok, false);
        });

        it('Should validate string exact size', function(){
            assert.strictEqual(v.len(1).test('a').ok, true);
            assert.strictEqual(v.len(2).test('b').ok, false);
        });

        it('Should validate string according to regexp', function(){
            assert.strictEqual(v.match(/a/).test('a').ok, true);
            assert.strictEqual(v.match(/abc/).test('d').ok, false);
        });

        it('Should validate string max size', function(){
            assert.strictEqual(v.max(1).test('a').ok, true);
            assert.strictEqual(v.max(2).test('bbb').ok, false);
        });

        it('Should validate string min size', function(){
            assert.strictEqual(v.min(1).test('a').ok, true);
            assert.strictEqual(v.min(2).test('b').ok, false);
        });

        it('Should validate string size range', function(){
            assert.strictEqual(v.between(1, 2).test('a').ok, true);
            assert.strictEqual(v.between(1, 2).test('bbb').ok, false);
        });

    });

    describe('Array Validations', function(){

        it('Should validate arrays have the exactly same items and order', function(){
            assert.strictEqual(v.dif(['a', 'b', 'c']).test(['a', 'b', 'f']).ok, true);
            assert.strictEqual(v.dif(['a', 'b', 'c']).test(['a', 'b', 'c']).ok, false);
        });

        it('Should validate array contain item', function(){
            assert.strictEqual(v.has('a').test(['a']).ok, true);
            assert.strictEqual(v.has('b').test(['a']).ok, false);
        });

        it('Should fail when matcher is not a Valinor', function(){
            assert.throws(() => v.every(true));
        });

        it('Should success if all elements of array match a given valinor', function(){
            assert.strictEqual(v.every(v.str).test(['a', 1]).ok, false);
            assert.strictEqual(v.every(v.num).test([2.4, 1, 0]).ok, true);

            let schema = v.obj.schema({
                a: v.bool,
                b: v.int
            });

            let data = [ { a: true, b: 8 }, { a: false } ];
            assert.strictEqual(v.every(schema).test(data).ok, false);
            data[1].b = 2;
            assert.strictEqual(v.every(schema).test(data).ok, true);
        });

        it('Should success if any elements of array match a given valinor', function(){
            assert.strictEqual(v.some(v.str).test([true, 1]).ok, false);
            assert.strictEqual(v.some(v.num).test([2.4, true, 'str']).ok, true);
        });

    });

    describe('Object Validations', function(){

        it('Should validate object has key', function(){
            assert.strictEqual(v.has('a').test({a: true}).ok, true);
            assert.strictEqual(v.has('b').test({a: true}).ok, false);
        });

        it('Should fail when schema fields are not Valinors', function(){
            assert.throws(() => v.schema({ a: true }) );
        });

        it('Should validate object values as Valinors', function(){
            let subject = { a: true, b: 9 };
            let schema = v.obj.schema({
                a: v.bool,
                b: v.int.max(9),
            });
            assert.strictEqual(schema.test(subject).ok, true);
            subject.c = 'hey';
            assert.strictEqual(schema.test(subject).ok, false);
        });

        it('Should recursively validate Nested Schema Valinors', function(){
            let subject = { m: { a: true, b: 9 }, c: '' };
            let schema = v.obj.schema({
                m: v.schema({
                    a: v.bool,
                    b: v.int.max(9)
                }),
                c: v.opt.num
            });

            assert.strictEqual(schema.test(subject).ok, true);

            subject.m = undefined;
            assert.strictEqual(schema.test(subject).ok, false);
        });

        it('Should ignore fields not present in schema', function(){
            let subject = { m: { a: true, b: null }, c: 14 };
            let schema = v.obj.schema({
                m: v.schema({
                    a: v.bool,
                    b: v.opt.def(3).int.max(9)
                })
            }, { ignoreUnknown: true });
            let r = schema.test(subject);
            assert.strictEqual(typeof r.final?.c, 'undefined');
        });

        it('Should complain on fields not present in schema', function(){
            let subject = { m: { a: true, b: null }, c: 14 };
            let schema = v.obj.schema({
                m: v.schema({
                    a: v.bool,
                    b: v.opt.def(3).int.max(9)
                })
            });
            let r = schema.test(subject);
            assert.strictEqual(r.errs[0].c, 'unknown-property');
        });

        it('Should replace final with details', function(){
            let subject = { m: { a: true, b: null }, c: '' };
            let schema = v.obj.schema({
                m: v.schema({
                    a: v.bool,
                    b: v.opt.def(3).int.max(9)
                }),
                c: v.opt.num.def(15.6)
            });
            let r = schema.test(subject);

            assert.strictEqual(r.final.m.b, 3);
            assert.strictEqual(r.final.c, 15.6);
        });

        it('Should mutate input date in a pipeline style', function(){
            let obj = { foo: 'bar' };

            assert.throws(() => v.alter());

            let r = v.schema({
                foo: v.str.alter(() => 24).num
            }).test(obj);

            assert.strictEqual(r.final.foo, 24);
            assert.strictEqual(obj.foo, 'bar');
        });

        it('Should success if ALL values of object match a given valinor', function(){
            assert.strictEqual(v.values(v.str).test({ a: 'a', b: 1 }).ok, false);
            assert.strictEqual(v.values(v.num).test({ a: 2.4, b: 1, c: 0 }).ok, true);

            const schema = v.obj.schema({
                a: v.bool,
                b: v.int
            });

            const data = { z: { a: true, b: 8 }, y: { a: false } };
            assert.strictEqual(v.values(schema).test(data).ok, false);
            const data2 = { z: { a: true, b: 8 }, y: { a: false, b: 2 } };
            assert.strictEqual(v.values(schema).test(data2).ok, true);
        });

        it('Should success if ALL keys of object match a given valinor', function(){
            assert.strictEqual(v.keys(v.in('a')).test({ a: 'a', b: 1 }).ok, false);
            assert.strictEqual(v.keys(v.in('ab')).test({ a: 'a', b: 1 }).ok, true);
        });

    });

});
