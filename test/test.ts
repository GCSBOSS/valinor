import { assertEquals, assert } from 'https://deno.land/std@0.192.0/testing/asserts.ts';
import { v } from '../lib/main.ts';

Deno.test('Should ignore other validations when optional field is blank', function(){
    assertEquals(v.opt.int.test('').ok, true);
});

Deno.test('Should abort single valinor chain when rule check fails', function(){
    assertEquals(v.str.between(2, 4).test('123').ok, true);
    assertEquals(v.str.between(2, 4).test(undefined).ok, false);
});

Deno.test('Types - Should validate integers', function(){
    assertEquals(v.int.test(0).ok, true);
    assertEquals(v.int.test('a').ok, false);
    assertEquals(v.not(v.int).test('a').ok, true);
    assertEquals(v.not(v.int).test(1).ok, false);
});

Deno.test('Types - Should validate numeric strings', function(){
    assertEquals(v.numeric.num.test(15).ok, true);
    assertEquals(v.numeric.num.test('12.5').ok, true);
    assertEquals(typeof v.numeric.test('100').final, 'number');
    assertEquals(v.numeric.test('a').ok, false);
});

Deno.test('Types - Should validate booleans', function(){
    assertEquals(v.bool.test(true).ok, true);
    assertEquals(v.bool.test('a').ok, false);
});

Deno.test('Types - Should validate boolean-like values', function(){
    assertEquals(v.boolLike.bool.test(true).ok, true);
    assertEquals(v.boolLike.bool.test('true').ok, true);
    assertEquals(v.boolLike.bool.test('false').ok, true);
    assertEquals(v.boolLike.bool.test('1').ok, true);
    assertEquals(v.boolLike.bool.test('0').ok, true);
    assertEquals(v.boolLike.bool.test(1).ok, true);
    assertEquals(v.boolLike.bool.test(0).ok, true);
    assertEquals(typeof v.boolLike.test('false').final, 'boolean');
    assertEquals(v.boolLike.test('2').ok, false);
    assertEquals(v.boolLike.test(2).ok, false);
    assertEquals(v.boolLike.test('foobar').ok, false);
});

Deno.test('Types - Should validate strings', function(){
    assertEquals(v.str.test('a').ok, true);
    assertEquals(v.str.test(true).ok, false);
});

Deno.test('Types - Should validate numbers', function(){
    assertEquals(v.num.test(1.2).ok, true);
    assertEquals(v.num.test('a').ok, false);
});

Deno.test('Types - Should validate dates', function(){
    assertEquals(v.date.test('12/12/2019').ok, true);
    assertEquals(v.date.test('a').ok, false);
    assertEquals(v.date.test('12/12/2019').final!.constructor.name, 'Date');
});

Deno.test('Types - Should validate ISO string dates', function(){
    assertEquals(v.date.test('2019-10-10T10:10:10Z').ok, true);
    assertEquals(v.date.test('2019-10-10T10:10:10Z').final!.constructor.name, 'Date');
    assertEquals(v.date.test('2019-24-10T10:10:10Z').ok, false);
    assertEquals(v.date.test('2019-xxxxxxx').ok, false);
});

Deno.test('Types - Should validate objects', function(){
    assertEquals(v.obj.test({}).ok, true);
    assertEquals(v.obj.test('a').ok, false);
});

Deno.test('Types - Should validate arrays', function(){
    assertEquals(v.arr.test([]).ok, true);
    assertEquals(v.arr.test('a').ok, false);
});

Deno.test('Primitives - Should validate number greater than', function(){
    assertEquals(v.gt(0).test(1).ok, true);
    assertEquals(v.gt(2.2).test(1).ok, false);
});

Deno.test('Primitives - Should validate number less than', function(){
    assertEquals(v.lt(2).test(1.5).ok, true);
    assertEquals(v.lt(1).test(1.5).ok, false);
});

Deno.test('Primitives - Should validate number greater than or equal to', function(){
    assertEquals(v.min(0).test(0).ok, true);
    assertEquals(v.min(2.2).test(1).ok, false);
});

Deno.test('Primitives - Should validate number less than or equal to', function(){
    assertEquals(v.max(2).test(2).ok, true);
    assertEquals(v.max(1).test(1.5).ok, false);
});

Deno.test('Primitives - Should validate difference', function(){
    assertEquals(v.dif('a').test(2).ok, true);
    assertEquals(v.dif('a').test('a').ok, false);
});

Deno.test('Primitives - Should validate if value is contained in array', function(){
    assertEquals(v.in(['a', 'b', 'c']).test('b').ok, true);
    assertEquals(v.in(['a', 'b']).test('c').ok, false);
    assertEquals(v.not(v.in(['a', 'c'])).test('b').ok, true);
    assertEquals(v.not(v.in(['a', 'b'])).test('a').ok, false);
});

Deno.test('Primitives - Should validate if value is key of object', function(){
    assertEquals(v.in({ a: true, b: 1, c: '' }).test('b').ok, true);
    assertEquals(v.in({ a: true, b: 1 }).test('c').ok, false);
    assertEquals(v.not(v.in({ a: true, c: '' })).test('b').ok, true);
    assertEquals(v.not(v.in({ a: true, b: 1 })).test('a').ok, false);
});

Deno.test('Date - Should validate equal dates', function(){
    const d = new Date(new Date().getTime() + 60000);
    assertEquals(v.dif(d).test(new Date()).ok, true);
    assertEquals(v.dif(d).test(d).ok, false);
});

Deno.test('Date - Should validate past dates', function(){
    assertEquals(v.past.test('2000-10-10').ok, true);
    assertEquals(v.past.test('9999-02-02').ok, false);
});

Deno.test('Date - Should validate future dates', function(){
    assertEquals(v.future.test('9999-02-02').ok, true);
    assertEquals(v.future.test('2000-10-10').ok, false);
});

Deno.test('Date - Should validate dates in the present day', function(){
    const od = new Date(new Date().getTime() + 60000);
    assertEquals(v.today.test(od).ok, true);
    assertEquals(v.today.test('2000-10-10').ok, false);
});

Deno.test('Date - Should validate date before another', function(){
    const d = new Date();
    assertEquals(v.max('9999-02-02').test(d).ok, true);
    assertEquals(v.max('2000-10-10').test(d).ok, false);
});

Deno.test('Date - Should validate date after another', function(){
    const d = new Date();
    assertEquals(v.min('2000-10-10').test(d).ok, true);
    assertEquals(v.min('9999-02-02').test(d).ok, false);
});

Deno.test('Date - Should validate date in range', function(){
    const d = new Date(new Date().getTime() - 60000);
    assertEquals(v.between('2000-10-10', '9999-02-02').test(d).ok, true);
    assertEquals(v.between(new Date(), '9999-02-02').test(d).ok, false);
});

Deno.test('String - Should validate substrings', function(){
    assertEquals(v.has('a').test('bab').ok, true);
    assertEquals(v.has('b').test('cac').ok, false);
});

Deno.test('String - Should validate superstrings', function(){
    assertEquals(v.in('bab').test('a').ok, true);
    assertEquals(v.in('cac').test('b').ok, false);
    assertEquals(v.not(v.in('bab')).test('c').ok, true);
    assertEquals(v.not(v.in('dad')).test('d').ok, false);
});

Deno.test('String - Should validate string exact size', function(){
    assertEquals(v.len(1).test('a').ok, true);
    assertEquals(v.len(2).test('b').ok, false);
});

Deno.test('String - Should validate string according to regexp', function(){
    assertEquals(v.match(/a/).test('a').ok, true);
    assertEquals(v.match(/abc/).test('d').ok, false);
});

Deno.test('String - Should validate string max size', function(){
    assertEquals(v.max(1).test('a').ok, true);
    assertEquals(v.max(2).test('bbb').ok, false);
});

Deno.test('String - Should validate string min size', function(){
    assertEquals(v.min(1).test('a').ok, true);
    assertEquals(v.min(2).test('b').ok, false);
});

Deno.test('String - Should validate string size range', function(){
    assertEquals(v.between(1, 2).test('a').ok, true);
    assertEquals(v.between(1, 2).test('bbb').ok, false);
});

Deno.test('Array - Should validate arrays have the exactly same items and order', function(){
    assertEquals(v.dif(['a', 'b', 'c']).test(['a', 'b', 'f']).ok, true);
    assertEquals(v.dif(['a', 'b', 'c']).test(['a', 'b', 'c']).ok, false);
});

Deno.test('Array - Should validate array contain item', function(){
    assertEquals(v.has('a').test(['a']).ok, true);
    assertEquals(v.has('b').test(['a']).ok, false);
});

Deno.test('Array - Should success if all elements of array match a given valinor', function(){
    assertEquals(v.every(v.str).test(['a', 1]).ok, false);
    assertEquals(v.every(v.num).test([2.4, 1, 0]).ok, true);

    const schema = v.obj.schema({
        a: v.bool,
        b: v.int
    });

    const data = [ { a: true, b: 8 }, { a: false } ];
    assertEquals(v.every(schema).test(data).ok, false);
    data[1].b = 2;
    assertEquals(v.every(schema).test(data).ok, true);
});

Deno.test('Array - Should success if any elements of array match a given valinor', function(){
    assertEquals(v.some(v.str).test([true, 1]).ok, false);
    assertEquals(v.some(v.num).test([2.4, true, 'str']).ok, true);
});

Deno.test('Object - Should validate object has key', function(){
    assertEquals(v.has('a').test({a: true}).ok, true);
    assertEquals(v.has('b').test({a: true}).ok, false);
});

Deno.test('Object - Should validate object values as Valinors', function(){
    const subject: Record<string, unknown> = { a: true, b: 9 };
    const schema = v.obj.schema({
        a: v.bool,
        b: v.int.max(9)
    });
    assertEquals(schema.test(subject).ok, true);
    subject.c = 'hey';
    assertEquals(schema.test(subject).ok, false);
});

Deno.test('Object - Should recursively validate Nested Schema Valinors', function(){
    const subject = { m: { a: true, b: 9 }, c: '' };
    const schema = v.obj.schema({
        m: v.schema({
            a: v.bool,
            b: v.int.max(9)
        }),
        c: v.opt.num
    });

    assertEquals(schema.test(subject).ok, true);

    const subject2 = { c: '' };
    assertEquals(schema.test(subject2).ok, false);
});

Deno.test('Object - Should ignore fields not present in schema', function(){
    const subject = { m: { a: true, b: null }, c: 14 };
    const schema = v.obj.schema({
        m: v.schema({
            a: v.bool,
            b: v.opt.def(3).int.max(9)
        })
    }, { ignoreUnknown: true });
    const r = schema.test(subject);
    assert(r.ok);
    assertEquals(typeof (r.final as unknown as typeof subject)?.c!, 'undefined');
});

Deno.test('Object - Should complain on fields not present in schema', function(){
    const subject = { m: { a: true, b: null }, c: 14 };
    const schema = v.obj.schema({
        m: v.schema({
            a: v.bool,
            b: v.opt.def(3).int.max(9)
        })
    });
    const r = schema.test(subject);
    assertEquals((r.errs?.[0] as unknown as { c: string }).c, 'unknown-property');
});

Deno.test('Object - Should replace final with details', function(){
    const subject = { m: { a: true, b: null }, c: '' };
    const schema = v.obj.schema({
        m: v.schema({
            a: v.bool,
            b: v.opt.def(3).int.max(9)
        }),
        c: v.opt.num.def(15.6)
    });
    const r = schema.test(subject);

    assertEquals(r.final?.m.b, 3);
    assertEquals(r.final?.c, 15.6);
});

Deno.test('Object - Should mutate input data in a pipeline style', function(){
    const obj = { foo: 'bar' };

    const r = v.schema({
        foo: v.str.alter(() => 24).num
    }).test(obj);

    assertEquals(r.final?.foo, 24);
    assertEquals(obj.foo, 'bar');
});

Deno.test('Object - Should success if ALL values of object match a given valinor', function(){
    assertEquals(v.values(v.str).test({ a: 'a', b: 1 }).ok, false);
    assertEquals(v.values(v.num).test({ a: 2.4, b: 1, c: 0 }).ok, true);

    const schema = v.obj.schema({
        a: v.bool,
        b: v.int
    });

    const data = { z: { a: true, b: 8 }, y: { a: false } };
    assertEquals(v.values(schema).test(data).ok, false);
    const data2 = { z: { a: true, b: 8 }, y: { a: false, b: 2 } };
    assertEquals(v.values(schema).test(data2).ok, true);
});

Deno.test('Object - Should success if ALL keys of object match a given valinor', function(){
    assertEquals(v.keys(v.in('a')).test({ a: 'a', b: 1 }).ok, false);
    assertEquals(v.keys(v.in('ab')).test({ a: 2.4, b: 1 }).ok, true);
});
