'use strict';
Object.defineProperty(exports, '__esModule', { value: true });
exports.v = void 0;
class RuleOperandError extends Error {
    constructor(rule, operand) {
        const s = typeof operand === 'object' ? 'type \'object' : '\'' + operand;
        super(`Invalid operand ${s}' for '${rule}' rule`);
    }
}
function arrayEqual(a, b) {
    if(a.length !== b.length)
        return false;
    for(const i in a)
        if(a[i] !== b[i])
            return false;
    return true;
}
function checkLength(input, rule) {
    if(typeof input != 'string' && typeof input != 'number' &&
        !(input instanceof Date) && !Array.isArray(input))
        throw new RuleOperandError(rule, input);
    const len = input?.length;
    return Number.isInteger(len)
        ? len
        : input;
}
function isEmpty(value) {
    const t = typeof value;
    const empty = t === 'string' && value.trim() === '';
    return empty || t === 'undefined' || value === null;
}
class Valinor {
    rules;
    optional;
    default;
    constructor() {
        this.rules = [{ fn: val => !isEmpty(val), name: 'required' }];
    }
    /**
     * Run the Valinor rules over the `input` value
     * @param input
     * @returns validation result
     */
    test(input) {
        if(this.optional && isEmpty(input))
            return { ok: true, final: this.default };
        return this.applyRules(input);
    }
    /**
     * Set a default value to be used when the input value is blank for an optional Valinor
     * @param value
     * @returns the Valinor intance itself
     */
    def(value) {
        this.default = value;
        return this;
    }
    /**
     * Allow input value to be blank. Values considered blank are:
     * - `undefined`
     * - `null`
     * - `''`
     * - strings made only of white-space
     * @returns the Valinor intance itself
     */
    get opt() {
        this.optional = true;
        return this;
    }
    /**
     * Validate if the properties of an object match a given set of rules
     * @param spec &mdash; an object containing of allowed keys and corresponding rules
     * @param opts &mdash; aditional validation options
     * @returns the Valinor intance itself
     */
    schema(spec, opts = {}) {
        if(typeof spec != 'object' || Object.values(spec).some(va => !(va instanceof Valinor)))
            throw new TypeError('\'spec\' must be a Valinor instance');
        this.rules.push({
            fn: input => {
                let ok = true;
                if(typeof input !== 'object')
                    return false;
                const final = {};
                const errs = {};
                for(const key in spec) {
                    const result = spec[key].test(input[key]);
                    if(result.ok == false) {
                        errs[key] = result.errs;
                        ok = false;
                    }
                    else
                        final[key] = result.final;
                }
                if(!opts?.ignoreUnknown)
                    for(const key in input)
                        if(!(key in spec)) {
                            ok = false;
                            errs[key] = 'unknown-property';
                        }
                const r = { ok };
                if(ok)
                    r.final = final;
                else
                    r.props = errs;
                return r;
            },
            name: 'schema',
        });
        return this;
    }
    /**
     * Validate whether all elements of an array match a given set of rules
     * @param spec
     * @returns the Valinor intance itself
     */
    every(spec) {
        if(!(spec instanceof Valinor))
            throw new TypeError('\'spec\' must be a Valinor instance');
        this.rules.push({
            fn: input => {
                let ok = true;
                if(!Array.isArray(input))
                    return false;
                const final = [];
                const errs = [];
                for(const sub of input) {
                    const result = spec.test(sub);
                    if(result.ok == false) {
                        errs.push(result.errs);
                        ok = false;
                    }
                    else
                        final.push(result.final);
                }
                return {
                    ok,
                    props: { errs },
                    final
                };
            },
            name: 'every'
        });
        return this;
    }
    /**
     * Validate whether any of the elements of an array match a given set of rules
     * @param spec
     * @returns the Valinor intance itself
     */
    some(spec) {
        if(!(spec instanceof Valinor))
            throw new TypeError('\'spec\' must be a Valinor instance');
        this.rules.push({
            fn: input => {
                let ok = false;
                if(!Array.isArray(input))
                    return false;
                const final = [];
                const errs = [];
                for(const sub of input) {
                    const result = spec.test(sub);
                    if(result.ok == false) 
                        errs.push(result.errs);
                    
                    else{
                        final.push(result.final);
                        ok = true;
                    }
                }
                const r = { ok };
                if(ok)
                    r.final = final;
                else
                    r.props = { errs };
                return r;
            },
            name: 'some'
        });
        return this;
    }
    /**
     * Validate whether all keys of an object match a given set of rules
     * @param spec
     * @returns the Valinor intance itself
     */
    keys(spec) {
        if(!(spec instanceof Valinor))
            throw new TypeError('\'spec\' must be a Valinor instance');
        this.rules.push({
            fn: input => {
                let ok = true;
                if(!input || typeof input != 'object')
                    return false;
                const invalid = [];
                for(const idx in input) {
                    const result = spec.test(idx);
                    if(result.ok == false) {
                        invalid.push(idx);
                        ok = false;
                    }
                }
                return {
                    ok,
                    props: { invalid }
                };
            },
            name: 'keys'
        });
        return this;
    }
    /**
     * Validate whether all values of an object match a given set of rules
     * @param spec
     * @returns the Valinor intance itself
     */
    values(spec) {
        if(!(spec instanceof Valinor))
            throw new TypeError('\'spec\' must be a Valinor instance');
        this.rules.push({
            fn: input => {
                let ok = true;
                if(!input || typeof input != 'object')
                    return false;
                const errs = {};
                for(const idx in input) {
                    const result = spec.test(input[idx]);
                    if(result.ok == false) {
                        errs[idx] = result.errs;
                        ok = false;
                    }
                }
                return {
                    ok,
                    props: errs
                };
            },
            name: 'keys'
        });
        return this;
    }
    /**
     * Transforms the input value using a function before performing the next validation steps
     * @param fn &mdash; a function whose return will be used as the input value afterwards
     * @returns the Valinor intance itself
     */
    alter(fn) {
        if(typeof fn != 'function')
            throw new TypeError('\'fn\' must be a function');
        this.rules.push({
            fn: input => ({
                ok: true,
                final: fn(input)
            }),
            name: 'alter',
        });
        // return this.proxy;
        return this;
    }
    applyRules(input) {
        const errs = [];
        let final = input;
        for(const rule of this.rules) {
            const rulePrevResult = rule.fn(final);
            const ruleResult = typeof rulePrevResult == 'boolean'
                ? { ok: rulePrevResult }
                : rulePrevResult;
            if(typeof ruleResult?.final != 'undefined')
                final = ruleResult.final;
            if(ruleResult.ok === false) {
                errs.push({ ...ruleResult.props, rule: rule.name });
                break;
            }
        }
        const r = { ok: errs.length == 0 };
        if(r.ok)
            r.final = final;
        else
            r.errs = errs;
        return r;
    }
    /**
     * Validate whether the input value is an Integer
     * @returns the Valinor intance itself
     */
    get int() {
        this.rules.push({
            name: 'int',
            fn: input => Number.isInteger(input)
        });
        return this;
    }
    /**
     * Validate whether the input value is an `boolean`
     * @returns the Valinor intance itself
     */
    get bool() {
        this.rules.push({
            name: 'bool',
            fn: input => typeof input === 'boolean'
        });
        return this;
    }
    /**
     * Validate whether the input value is a boolean, 'true', 'false', '1, 1, '0' or 0
     * and transforms the value in a boolean.
     * @returns the Valinor intance itself
     */
    get boolLike() {
        this.rules.push({
            name: 'boolLike',
            fn: input => {
                const final = input === 'false' || input === '0' ? false : Boolean(input);
                return {
                    ok: typeof input === 'boolean' || input === 'true' || input === 'false'
                        || input === 1 || input === 0 || input === '1' || input === '0',
                    final
                };
            }
        });
        return this;
    }
    /**
     * Validate whether the input value is a `string`
     * @returns the Valinor intance itself
     */
    get str() {
        this.rules.push({
            name: 'str',
            fn: input => typeof input === 'string'
        });
        return this;
    }
    /**
     * Validate whether the input value is a `number`
     * @returns the Valinor intance itself
     */
    get num() {
        this.rules.push({
            name: 'num',
            fn: input => typeof input === 'number'
        });
        return this;
    }
    /**
     * Validate whether the input value is a `number`, or a numeric string
     * @returns the Valinor intance itself
     */
    get numeric() {
        this.rules.push({
            name: 'numeric',
            fn: input => {
                const n = Number(input);
                return {
                    ok: typeof input === 'number' || typeof input == 'string' && !isNaN(n) && !isNaN(parseFloat(input)),
                    final: n
                };
            }
        });
        return this;
    }
    /**
     * Validate whether the input value is any of:
     * - a `Date` instance
     * - a valid date string
     * - a valid unix timestamp number in milliseconds
     *
     * Also transforms the value into a `Date` object
     * @returns the Valinor intance itself
     */
    get date() {
        this.rules.push({
            name: 'date',
            fn: input => {
                const dt = new Date(input);
                return {
                    ok: !isNaN(Number(dt)),
                    final: dt
                };
            }
        });
        return this;
    }
    /**
     * Validate whether the input value is an ISO date string
     * Also transforms the value into a `Date` object
     * @returns the Valinor intance itself
     */
    get isoDate() {
        this.rules.push({
            name: 'isoDate',
            fn: input => {
                const dt = new Date(input);
                const ISO_REGEXP = /\d{4}-[01]\d-[0-3]\dT[0-2]\d:[0-5]\d:[0-5]\d(\.\d+)?/;
                return {
                    ok: ISO_REGEXP.test(String(input)) && !isNaN(Number(dt)),
                    final: dt
                };
            }
        });
        return this;
    }
    /**
     * Validate whether the input value is an `object` that is not an `Array` or nullish
     * @returns the Valinor intance itself
     */
    get obj() {
        this.rules.push({
            name: 'obj',
            fn: input => Boolean(input) && typeof input == 'object' && !Array.isArray(input)
        });
        return this;
    }
    /**
     * Validate whether the input value is an `Array`
     * @returns the Valinor intance itself
     */
    get arr() {
        this.rules.push({
            name: 'arr',
            fn: input => Array.isArray(input)
        });
        return this;
    }
    /**
     * Validate whether the input value is a date in the future.
     * See also the `date` rule for a description of valid date inputs
     * @returns the Valinor intance itself
     */
    get future() {
        this.rules.push({
            name: 'future',
            fn: input => new Date(input) > new Date()
        });
        return this;
    }
    /**
     * Validate whether the input value is a date in the past.
     * See also the `date` rule for a description of valid date inputs
     * @returns the Valinor intance itself
     */
    get past() {
        this.rules.push({
            name: 'past',
            fn: input => new Date(input) < new Date()
        });
        return this;
    }
    /**
     * Validate whether the input value is a date in the present day.
     * See also the `date` rule for a description of valid date inputs
     * @returns the Valinor intance itself
     */
    get today() {
        this.rules.push({
            name: 'today',
            fn: input => {
                const d = new Date(input);
                const n = new Date();
                return d.getDate() == n.getDate() &&
                    d.getMonth() == n.getMonth() &&
                    d.getFullYear() == n.getFullYear();
            }
        });
        return this;
    }
    in(haystack) {
        this.rules.push({
            name: 'in',
            fn: input => {
                let ok = false;
                let hs;
                if(Array.isArray(haystack)) {
                    hs = haystack.join(',');
                    ok = haystack.includes(input);
                }
                else if(typeof haystack === 'object') {
                    hs = Object.keys(haystack).join(',');
                    ok = String(input) in haystack;
                }
                else if(typeof haystack === 'string') {
                    hs = haystack;
                    ok = haystack.indexOf(String(input)) >= 0;
                }
                else
                    throw new RuleOperandError('in', haystack);
                return {
                    ok,
                    props: { haystack: hs }
                };
            }
        });
        return this;
    }
    /**
     * Validate whether the input value matches the given regular expression
     * @param regex
     * @returns the Valinor intance itself
     */
    match(regex) {
        this.rules.push({
            name: 'match',
            fn: input => {
                return {
                    ok: regex.test(String(input)),
                    props: {
                        pattern: String(regex)
                    }
                };
            }
        });
        return this;
    }
    /**
     * Validate whether the input value is different than a given value
     * @param value
     * @returns the Valinor intance itself
     */
    dif(value) {
        this.rules.push({
            name: 'dif',
            fn: input => {
                let ok = false;
                if(typeof value !== 'object')
                    ok = input !== value;
                else if(Array.isArray(value))
                    ok = !arrayEqual(input, value);
                else if(value instanceof Date)
                    ok = new Date(input).getTime() !== new Date(value).getTime();
                else
                    throw new RuleOperandError('dif', value);
                return { ok, props: { value } };
            }
        });
        return this;
    }
    /**
     * Validates whether the input value is greater than or equal to the given limit
     * @param limit
     * @returns the Valinor intance itself
     */
    min(limit) {
        this.rules.push({
            name: 'min',
            fn: input => {
                const len = checkLength(input, 'min');
                if(len instanceof Date)
                    limit = new Date(limit);
                return {
                    ok: len >= limit,
                    props: { limit }
                };
            }
        });
        return this;
    }
    /**
     * Validates whether the input value is smaller than the given limit
     * @param limit
     * @returns the Valinor intance itself
     */
    lt(limit) {
        this.rules.push({
            name: 'lt',
            fn: input => {
                const len = checkLength(input, 'lt');
                if(len instanceof Date)
                    limit = new Date(limit);
                return {
                    ok: len < limit,
                    props: { limit }
                };
            }
        });
        return this;
    }
    /**
     * Validates whether the input value is greater than the given limit
     * @param limit
     * @returns the Valinor intance itself
     */
    gt(limit) {
        this.rules.push({
            name: 'gt',
            fn: input => {
                const len = checkLength(input, 'gt');
                if(len instanceof Date)
                    limit = new Date(limit);
                return {
                    ok: len > limit,
                    props: { limit }
                };
            }
        });
        return this;
    }
    /**
     * Validates whether the input value is smaller than or equal to the given limit
     * @param limit
     * @returns the Valinor intance itself
     */
    max(limit) {
        this.rules.push({
            name: 'max',
            fn: input => {
                const len = checkLength(input, 'max');
                if(len instanceof Date)
                    limit = new Date(limit);
                return {
                    ok: len <= limit,
                    props: { limit }
                };
            }
        });
        return this;
    }
    /**
     * Validates whether the input value is inclusively between the given min and max
     * @param min
     * @param max
     * @returns the Valinor intance itself
     */
    between(min, max) {
        this.rules.push({
            name: 'between',
            fn: input => {
                const len = checkLength(input, 'between');
                if(len instanceof Date) {
                    min = new Date(min);
                    max = new Date(max);
                }
                return {
                    ok: len >= min && len <= max,
                    props: { min, max }
                };
            }
        });
        return this;
    }
    /**
     * Validate whether the input falie DOES NOT pass in the given rules.
     * This is basically a rule _negation_ feature
     * @param rules
     * @returns the Valinor intance itself
     */
    not(rules) {
        this.rules.push({
            name: 'not',
            fn: input => {
                const r = rules.applyRules(input);
                if(!r.ok)
                    return true;
                return {
                    ok: false,
                    errs: r.errs
                };
            }
        });
        return this;
    }
    /**
     * Validates whether the input value has the expected length
     * @param expected
     * @returns the Valinor intance itself
     */
    len(expected) {
        this.rules.push({
            name: 'len',
            fn: input => {
                const len = checkLength(input, 'len');
                return {
                    ok: len === expected,
                    props: { expected }
                };
            }
        });
        return this;
    }
    /**
     * Validate whether the input value contains the given needle.
     * The needle will be searched in the following manners depending on input value type:
     * - a `string`, check if needle is a substring
     * - a plain object, check if needle is a key that exists in the object
     * - an `Array`, check if needle is included in
     * @param needle
     * @returns the Valinor intance itself
     */
    has(needle) {
        this.rules.push({
            name: 'has',
            fn: input => {
                let ok = false;
                if(Array.isArray(input))
                    ok = input.includes(needle);
                else if(input && typeof input === 'object')
                    ok = String(needle) in input;
                else if(typeof input === 'string')
                    ok = input.indexOf(String(needle)) >= 0;
                else
                    throw new RuleOperandError('has', input);
                return { ok, needle };
            }
        });
        return this;
    }
}
/**
 * Entrypoint for setting up a valinor instance.
 *
 * Example usage:
 * ```
 * const { v } = require('valinor');
 *
 * // Validate one variable in place.
 * let result = v.int.between(0, 32).test(33);
 * console.log(result);
 *
 * // Reuse one valinor.
 * let goodPwd = v.str.min(8);
 * console.log(
 *     goodPwd.test('abc'),
 *     goodPwd.test('abcdefgh'),
 *     goodPwd.test(null)
 * );
 *
 * // Define a object schema valinor.
 * let schema = v.schema({
 *     name: v.str.match(/^[A-Z][a-z]+\ [A-Z][a-z]+$/),
 *     password: v.str.min(8),
 *     birth: v.date.min('2001-01-01').past
 * });
 *
 * result = schema.test({
 *     name: 'John Doe',
 *     password: 'abcdefgh',
 *     birth: new Date('1999-01-01')
 * });
 *
 * // Ignore empty/null values on optional valinors.
 * let schemaWithOpt = v.schema({
 *     firstName: v.str,
 *     lastName: v.opt.str
 * });
 *
 * console.log(schemaWithOpt.test({
 *     firstName: 'John',
 *     lastName: null
 * }));
 *
 * // Ensure you get only valdated keys of an object.
 * console.log(schemaWithOpt.test({
 *     firstName: 'John',
 *     unvalidated: 'This should not show up',
 *     lastName: 'Doe'
 * }).final);
 *
 * // Define default values to show up in final object instead of blank optionals
 * let schemaWithOpt = v.schema({
 *     firstName: v.str,
 *     lastName: v.opt.str.def('Doe')
 * });
 *
 * console.log(schemaWithOpt.test({
 *     firstName: 'John'
 * }).final);
 * ```
 */
exports.v = new Proxy({ fake: true }, {
    get(_obj, prop) {
        const newVal = new Valinor();
        const propOrVal = newVal[prop];
        return typeof propOrVal == 'function'
            ? propOrVal.bind(newVal)
            : propOrVal;
    }
});
